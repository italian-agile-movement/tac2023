fetch('components/OrganizerCard/organizer-card.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class OrganizerCard extends HTMLElement {
    constructor() {
      super();
      this.key = 'unknown';
    }

    // component attributes
    static get observedAttributes() {
      return ['key'];
    }

    // attribute change
    attributeChangedCallback(property, oldValue, newValue) {
      if (oldValue === newValue) return;
      this[property] = newValue;
    }

    // connect component
    connectedCallback() {
      const data = TAC_ORGANIZERS[this.key];
      const templateNode = parseHtml(html);
      if (data != null) {
        setOrganizerLink(templateNode, data.link);
        setOrganizerImage(templateNode, data.img, data.imgAlt);
        setOrganizerName(templateNode, data.name, data.link);
      }

      this.append(templateNode);
    }
  }

  // register component
  customElements.define('organizer-card', OrganizerCard);

  function setOrganizerName(templateNode, organizerName, organizerLink) {
    const nameEl = templateNode.getElementById('organizer-name');
    nameEl.innerText = organizerName;
    nameEl.href = organizerLink;
  }

  function setOrganizerImage(templateNode, imageUrl, imageAlt) {
    const imageEl = templateNode.getElementById('organizer-img');
    imageEl.src = imageUrl;
    imageEl.alt = imageAlt;
  }

  function setOrganizerLink(templateNode, organizerLink) {
    const imageEl = templateNode.getElementById('organizer-link');
    imageEl.href = organizerLink;
  }

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('organizer-card')
      .content.cloneNode(true);
  }

}
