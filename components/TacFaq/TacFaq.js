fetch('components/TacFaq/tac-faq.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacFaq extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-faq', TacFaq);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-faq')
      .content.cloneNode(true);
  }

}
