fetch('components/TacVenue/tac-venue.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacVenue extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-venue', TacVenue);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-venue')
      .content.cloneNode(true);
  }

}
