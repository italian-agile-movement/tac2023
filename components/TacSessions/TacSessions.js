fetch('components/TacSessions/tac-sessions.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacSessions extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-sessions', TacSessions);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-sessions')
      .content.cloneNode(true);
  }

}
