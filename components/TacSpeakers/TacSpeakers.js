fetch('components/TacSpeakers/tac-speakers.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacSpeakers extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-speakers', TacSpeakers);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-speakers')
      .content.cloneNode(true);
  }

}
