fetch('components/TacHeader/tac-header.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacHeader extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-header', TacHeader);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-header')
      .content.cloneNode(true);
  }

}
