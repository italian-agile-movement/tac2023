fetch('components/TacTickets/tac-tickets.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacTickets extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-tickets', TacTickets);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-tickets')
      .content.cloneNode(true);
  }

}
