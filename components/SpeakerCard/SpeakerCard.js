fetch('components/SpeakerCard/speaker-card.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class SpeakerCard extends HTMLElement {
    constructor() {
      super();
      this.key = 'unknown';
    }

    // component attributes
    static get observedAttributes() {
      return ['key'];
    }

    // attribute change
    attributeChangedCallback(property, oldValue, newValue) {
      if (oldValue === newValue) return;
      this[property] = newValue;
    }

    // connect component
    connectedCallback() {
      const data = TAC_SPEAKERS[this.key];
      const templateNode = parseHtml(html);
      if (data != null) {
        setSpeakerImage(templateNode, data.img, data.imgAlt);
        setSpeakerName(templateNode, this.key, data.name);
        setSpeakerSummary(templateNode, data.summary);
        setSpeakerSocial(templateNode, data.social || []);
      }

      this.append(templateNode);
    }
  }

  // register component
  customElements.define('speaker-card', SpeakerCard);

  function setSpeakerName(templateNode, speakerKey, speakerName) {
    const nameEl = templateNode.getElementById('speaker-name');
    nameEl.innerText = speakerName;
    nameEl.href = `speaker-details.html?key=${speakerKey}`;
  }

  function setSpeakerSummary(templateNode, speakerSummary) {
    const summaryEl = templateNode.getElementById('speaker-summary');
    summaryEl.innerText = speakerSummary;
  }

  function setSpeakerImage(templateNode, imageUrl, imageAlt) {
    const imageEl = templateNode.getElementById('speaker-img');
    imageEl.src = imageUrl;
    imageEl.alt = imageAlt;
  }

  function setSpeakerSocial(templateNode, socialArray) {
    const socialNode = templateNode.getElementById('speaker-social');
    socialNode.querySelectorAll('*').forEach((n) => n.remove());
    Array.from(socialArray).forEach((soc) => {
      appendSocialButton(socialNode, soc);
    });
  }

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('speaker-card')
      .content.cloneNode(true);
  }

  function appendSocialButton(socialNode, socialInfo) {
    const socIcon = document.createElement('i');
    socIcon.className = socialInfo.icon?.toString();

    const socAnch = document.createElement('a');
    socAnch.href = socialInfo.url?.toString();
    socAnch.target = '_blank';
    socAnch.appendChild(socIcon);

    socialNode.appendChild(socAnch);
  }
}
