fetch('components/TacKeynote/tac-keynote.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacKeynote extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-keynote', TacKeynote);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-keynote')
      .content.cloneNode(true);
  }

}
