fetch('components/TacSchedule/tac-schedule.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacSchedule extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-schedule', TacSchedule);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-schedule')
      .content.cloneNode(true);
  }

}
