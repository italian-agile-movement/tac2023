fetch('components/TacFooter/tac-footer.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacFooter extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-footer', TacFooter);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-footer')
      .content.cloneNode(true);
  }

}
