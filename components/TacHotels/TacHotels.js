fetch('components/TacHotels/tac-hotels.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacHotels extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-hotels', TacHotels);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-hotels')
      .content.cloneNode(true);
  }

}
