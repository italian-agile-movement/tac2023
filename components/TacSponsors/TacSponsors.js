fetch('components/TacSponsors/tac-sponsors.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class TacSponsors extends HTMLElement {

    // connect component
    connectedCallback() {
      const templateNode = parseHtml(html);
      this.append(templateNode);
    }

  }

  // register component
  customElements.define('tac-sponsors', TacSponsors);

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('tac-sponsors')
      .content.cloneNode(true);
  }

}
