fetch('components/SpeakerDetails/speaker-details.html')
  .then((stream) => stream.text())
  .then(define);

function define(html) {
  class SpeakerDetails extends HTMLElement {
    constructor() {
      super();
      this.key = 'unknown';
    }

    // component attributes
    static get observedAttributes() {
      return ['key'];
    }

    // attribute change
    attributeChangedCallback(property, oldValue, newValue) {
      if (oldValue === newValue) return;
      this[property] = newValue;
    }

    // connect component
    connectedCallback() {
      const data = this.extractSpeakerData();
      const templateNode = parseHtml(html);
      setSpeakerName(templateNode, data.name);
      setSpeakerSummary(templateNode, data.summary);
      setSpeakerImage(templateNode, data.img, data.imgAlt);
      setSpeakerSocial(templateNode, data.social || []);
      setSpeakerBio(templateNode, data.bio || []);

      this.append(templateNode);
    }

    extractSpeakerData() {
      const param = 'key';
      const paramValue = getUrlParamValue(window.location.href, param);
      return TAC_SPEAKERS[paramValue || this.key || ''];
    }
  }

  // register component
  customElements.define('speaker-details', SpeakerDetails);

  function setSpeakerName(templateNode, speakerName) {
    const nameEl = templateNode.getElementById('speaker-name');
    document.title = 'TAC - ' + speakerName;
    nameEl.innerText = speakerName;
  }

  function setSpeakerSummary(templateNode, speakerSummary) {
    const nameEl = templateNode.getElementById('speaker-summary');
    nameEl.innerText = speakerSummary;
  }

  function setSpeakerImage(templateNode, imageUrl, imageAlt) {
    const imageEl = templateNode.getElementById('speaker-img');
    imageEl.src = imageUrl;
    imageEl.alt = imageAlt;
  }

  function setSpeakerSocial(templateNode, socialArray) {
    const socialNode = templateNode.getElementById('speaker-social');
    socialNode.querySelectorAll('*').forEach((n) => n.remove());
    Array.from(socialArray).forEach((soc) => {
      appendSocialButton(socialNode, soc);
    });
  }

  function appendSocialButton(socialNode, socialInfo) {
    const socIcon = document.createElement('i');
    socIcon.className = socialInfo.icon?.toString();

    const socAnch = document.createElement('a');
    socAnch.href = socialInfo.url?.toString();
    socAnch.target = '_blank';
    socAnch.appendChild(socIcon);

    socialNode.appendChild(socAnch);
  }

  function setSpeakerBio(templateNode, bioArray) {
    const bioNode = templateNode.getElementById('speaker-bio');
    bioNode.querySelectorAll('*').forEach((n) => n.remove());
    Array.from(bioArray).forEach((bio) => {
      appendBioParagraph(bioNode, bio);
    });
  }

  function appendBioParagraph(bioNode, bioRow) {
    const bioParagraph = document.createElement('p');
    bioParagraph.innerText = bioRow?.toString();

    bioNode.appendChild(bioParagraph);
  }

  function parseHtml(html) {
    const parser = new DOMParser();
    return parser
      .parseFromString(html, 'text/html')
      .getElementById('speaker-details')
      .content.cloneNode(true);
  }

  function getUrlParamValue(url, key) {
    return new URL(url).searchParams.get(key);
  }
}
