/**
* Template Name: TheEvent
* Updated: Mar 10 2023 with Bootstrap v5.2.3
* Template URL: https://bootstrapmade.com/theevent-conference-event-bootstrap-template/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
*/
(function() {
  "use strict";

  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
      if (all) {
        selectEl.forEach(e => e.addEventListener(type, listener))
      } else {
        selectEl.addEventListener(type, listener)
      }
    }
    return selectEl;
  }

  /**
   * Easy on scroll event listener 
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
  * Scrolls to an element with header offset
  */
  const scrollto = (el) => {
    let header = select('#header')
    if (header) {
      let offset = header.offsetHeight

      if (!header.classList.contains('header-scrolled')) {
        offset -= 20
      }

      let elementPos = select(el)?.offsetTop ?? 0;
      window.scrollTo({
        top: elementPos - offset,
        behavior: 'smooth'
      })
    }
    return header;
  }

  /**
  * Exectues a callback at the specified interval until the function return truthy
  */
  function retryUntilTruthy(callback, interval, maxTries = 100) {
    let counter = 1;
    const intervalId = setInterval(() => {
      const result = callback();
      if (result || counter > maxTries)
        clearInterval(intervalId);
      counter++;
    }, interval);
  }

  /**
  * Add onscroll events when the page is loaded
  */
  window.addEventListener('load', () => {
    /**
    * Toggle .header-scrolled class to #header when page is scrolled
    */
    /**
    * Mobile nav toggle
    */
    retryUntilTruthy(() => {
      let selectHeader = select('#header');
      if (!selectHeader) return selectHeader;
      const headerScrolled = () => {
        if (window.scrollY > 100) {
          selectHeader.classList.add('header-scrolled')
        } else {
          selectHeader.classList.remove('header-scrolled')
        }
      }
      headerScrolled();
      onscroll(document, headerScrolled);
      return selectHeader;
    }, 50);
    
    /**
    * Back to top button
    */
    retryUntilTruthy(() => {
      let backtotop = select('.back-to-top');
      if (!backtotop) return backtotop;
      const toggleBacktotop = () => {
        if (window.scrollY > 100) {
          backtotop.classList.add('active')
        } else {
          backtotop.classList.remove('active')
        }
      }
      toggleBacktotop();
      onscroll(document, toggleBacktotop);
      return backtotop;
    }, 50);

    /**
    * Navbar links active state on scroll
    */
    retryUntilTruthy(() => {
      let navbarlinks = select('#navbar .scrollto', true)
      if (!navbarlinks || !navbarlinks.length) return false;
      const navbarlinksActive = () => {
        let position = window.scrollY + 200
        navbarlinks.forEach(navbarlink => {
          if (!navbarlink.hash) return
          let section = select(navbarlink.hash)
          if (!section) return
          if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
            navbarlink.classList.add('active')
          } else {
            navbarlink.classList.remove('active')
          }
        })
      }
      navbarlinksActive();
      onscroll(document, navbarlinksActive);
      return navbarlinks;
    }, 50);

  });


   /**
   * Add click listener after being sure that web components have been loaded
   */
  window.addEventListener('load', () => {
    /**
    * Mobile nav toggle
    */
    retryUntilTruthy(() => {
      return on('click', '.mobile-nav-toggle', function (e) {
        select('#navbar').classList.toggle('navbar-mobile')
        this.classList.toggle('bi-list')
        this.classList.toggle('bi-x')
      })
    }, 50);

    /**
    * Mobile nav dropdowns activate
    */
    retryUntilTruthy(() => {
      return on('click', '.navbar .dropdown > a', function (e) {
        if (select('#navbar').classList.contains('navbar-mobile')) {
          e.preventDefault()
          this.nextElementSibling.classList.toggle('dropdown-active')
        }
      }, true);
    }, 50);

    /**
    * Buy tickets select the ticket type on click
    */
    retryUntilTruthy(() => {
      return on('show.bs.modal', '#buy-ticket-modal', function (event) {
        select('#buy-ticket-modal #ticket-type').value = event.relatedTarget.getAttribute('data-ticket-type')
      });
    }, 50);


    /**
    * Scrool with ofset on links with a class name .scrollto
    */
    retryUntilTruthy(() => {
      return on('click', '.scrollto', function (e) {
        if (select(this.hash)) {
          e.preventDefault()

          let navbar = select('#navbar')
          if (navbar.classList.contains('navbar-mobile')) {
            navbar.classList.remove('navbar-mobile')
            let navbarToggle = select('.mobile-nav-toggle')
            navbarToggle.classList.toggle('bi-list')
            navbarToggle.classList.toggle('bi-x')
          }
          scrollto(this.hash)
        }
      }, true);
    }, 50);

  });

  /**
   * Scroll with ofset on page load with hash links in the url
   */
  window.addEventListener('load', () => {
    setTimeout(() => {
      if (window.location.hash) {
        retryUntilTruthy(() => {
          return scrollto(window.location.hash);
        }, 50);
      }
    }, 200);
    
  });

  /**
   * Initiate glightbox
   */
  window.addEventListener('load', () => {
    retryUntilTruthy(() => {
      const elements = select('.glightbox');
      if (!elements)
        return false;

      const glightbox = GLightbox({
        selector: '.glightbox',
        loop: true
      });

      return true;
    }, 50);
    
    
  });

  /**
   * Gallery Slider
   */
  new Swiper('.gallery-slider', {
    speed: 400,
    loop: true,
    centeredSlides: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      575: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      992: {
        slidesPerView: 5,
        spaceBetween: 20
      }
    }
  });

  /**
   * Initiate gallery lightbox 
   */
  const galleryLightbox = GLightbox({
    selector: '.gallery-lightbox'
  });

  /**
   * Animation on scroll
   */
  window.addEventListener('load', () => {
    AOS.init({
      duration: 1000,
      easing: 'ease-in-out',
      once: true,
      mirror: false
    })
  });

})()