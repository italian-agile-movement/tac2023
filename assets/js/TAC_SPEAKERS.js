(function () {
  'use strict';

  window.TAC_SPEAKERS = {
    andreaProvaglio: {
      img: 'assets/img/speakers/low-res/Andrea_Provaglio.jpeg',
      imgAlt: 'Speaker Andrea Provaglio',
      name: 'Andrea Provaglio',
      summary: 'Executive Business Agility Advisor',
      bio: [
        `Affianca aziende ed organizzazioni nel ripensare le proprie dinamiche organizzative per fare business più efficacemente nel 21° secolo.
         Dal top management ai team operativi, svolge attività di coaching, consulenza e affiancamento “on the job".
         Molto interessato ai temi della leadership distribuita, lavora su strutture organizzative non convenzionali, comunicazione, apprendimento e miglioramento continuo.
    
         Da 12 anni si occupa di agilità in Italia e all'estero e ha collaborato, in tre diversi continenti, con organizzazioni che vanno dalla FAO (Nazioni Unite) fino a start-up piccole e dinamiche.
         Ha lavorato per quattro anni negli Stati Uniti con un visto di tipo O-1 per “abilità straordinarie nelle scienze”.
    
         Come parte delle regolari attività, gli piace condividere le sue esperienze partecipando come speaker alle  principali conferenze internazionali del settore.`
      ],
      social: [
        {
          icon: 'bi bi-twitter',
          url: 'https://twitter.com/andreaprovaglio',
        },
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/provaglio/',
        },
        {
          icon: 'bi bi-globe-americas',
          url: 'http://andreaprovaglio.com/about',
        }
      ],
    },
    alessandroGiardina: {
      img: 'assets/img/speakers/low-res/alessandro-giardina.jpg',
      imgAlt: 'Speaker Alessandro Giardina',
      name: 'Alessandro Giardina',
      summary: 'Agile Delivery Manager @ Intré',
      bio: [
        `Ho iniziato la mia carriera come sviluppatore alla fine degli anni '90, durante gli studi in Ingegneria del Software, e negli anni ho avuto l'opportunità di vivere diversi ruoli come Team Leader, Product Owner, Scrum Master e Agile Coach, contribuendo a realizzare soluzioni in molteplici mercati: eCommerce, prenotazioni alberghiere, gestionali per negozi e piccole aziende, sistemi per la sanità pubblica, eLearning. Sono un praticante entusiasta delle Metodologie Agili. Mi piace sempre imparare cose nuove, lavorare in ambienti complessi ma collaborativi nei quali le persone fanno la differenza.
          Sono un team player, ma la cosa che adoro di più è far crescere e lavorare bene insieme le persone.
          Nel tempo libero organizzo conferenze, leggo, restauro moto d'epoca, cucino, e gioco con le mie figlie.`,
      ],
      social: [
        {
          icon: 'bi bi-twitter',
          url: 'https://twitter.com/IdnGarda/',
        },
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/idngarda/',
        },
        {
          icon: 'bi bi-globe-americas',
          url: 'https://www.intre.it/?s=giardina',
        }
      ],
    },
    almaDelCampo: {
      img: 'assets/img/speakers/low-res/alma-del-campo.jpg',
      imgAlt: 'Speaker Alma del Campo',
      name: 'Alma del Campo',
      summary: 'Membro fondatore di Agile at School',
      bio: [
        `Nata nel 1976 a Madrid, si appassiona fin da piccola alla tecnologia, conseguendo la laurea in Ingegneria Informatica al Politecnico di Madrid con specializzazione in gestione e qualità del software.
        Nel 2005 entra in contatto per la prima volta con l'agilità. Da lì nasce la sua passione per questa nuova modalità di approcciare il mondo del project management.
        Da sempre, si mostra di gran interesse per l'educazione partecipando in associazioni giovanili e unendo il lavoro in ambito tecnologico con l'esperienza nell'educazione di adulti ed adolescenti erogando corsi e workshop a diverse entità pubbliche e private.
        Questo mix di passioni si incrociano nel 2019 con la diffusione dell'agilità nei contesti educativi.`,
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/alma-maria-del-campo-vara-1497781a/',
        },
        {
          icon: 'bi bi-globe-americas',
          url: 'https://www.linkedin.com/company/agile-at-school/?viewAsMember=true',
        }
      ],
    },
    jacopoRomei: {
      img: 'assets/img/speakers/low-res/jacopo-romei.jpg',
      imgAlt: 'Speaker Jacopo Romei',
      name: 'Jacopo Romei',
      summary: 'Freelance strategy consultant',
      bio: [
        `In 2003 I spotted new agile ways to develop software and, as a young entrepreneur I gave them a try. I introduced agile in eBay Italia, Siemens and FAO but in 2011 I realized the problem often is in organisations' governance and in the agreements we negotiate. This led me to become a member of the board of European Organisation Design Forum (EODF).

        Usually I get hired by people who want to waste less time, money and enthusiasm. I help those people working together in better ways because that's a big chunk of our lives.

        I wrote a few books: “Pro PHP Refactoring”, a TDD chapter in “PHP Best Practices” and “Extreme Contracts” in 2017, on contracts and negotiation in knowledge work. I also contributed to a couple of books written by Jurgen Appelo.

        I am among the founders of ALE Agile Lean Europe network, PHP User Group Roma and Arduino User Group Roma.

        I've got a sailing license because of the sea, a glider pilot license because of the sky and I am an LSP facilitator certified by Lucio Margulis.`,
      ],
      social: [
        {
          icon: 'bi bi-twitter',
          url: 'https://twitter.com/jacoporomei',
        },
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/jacoporomei/',
        }
      ],
    },
    francescoFullone: {
      img: 'assets/img/speakers/low-res/francesco-fullone.png',
      imgAlt: 'Speaker Francesco Fullone',
      name: 'Francesco Fullone',
      summary: 'consultant, entrepreneur, community leader and advisor',
      bio: [
        `I'm Francesco Fullone, a business designer, consultant, and mentor and investor in technology companies focusing on sustainability or social impact.

        I help companies (in any stage) to better define their business propositions to create options to validate them, and I advise them to organize themselves better to reach their goals.

        I founded, and I contribute to Apropos, a company that helps communities, companies, and local groups organize events and workshops. If you attended an international conference about web technologies in Italy, you met me as a speaker, organizer, or attendee.

        I founded companies such as Digitiamo to create NLP and AI-related products, Carma, to work on the next food technologies. In the past, Ideato, where I contributed to its growth for more than ten years, creating an innovative and quality reference for the development agencies in Italy.

        I'm the president of the GrUSP, a no-profit association that encourages best practices in web development since 2002.`,
      ],
      social: [
        {
          icon: 'bi bi-twitter',
          url: 'http://twitter.com/fullo',
        },
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/fullo/',
        },
        {
          icon: 'bi bi-globe-americas',
          url: 'https://www.fullo.net/',
        }
      ],
    },
    niccoloBattezzati: {
      img: 'assets/img/speakers/low-res/niccolo-battezzati.jpeg',
      imgAlt: 'Speaker Niccolò Battezzati',
      name: 'Niccolò Battezzati',
      summary: 'Software engineer and Agile advisor at Argotec',
      bio: [
        `Niccolò Battezzati è nato a Torino nel 1983, ha frequentato il Politecnico di Torino dove ha conseguito il dottorato di ricerca in Ingegneria Informatica e dei Sistemi lavorando sull'affidabilità dei sistemi elettronici in missioni spaziali.

         Ha poi lavorato per circa dieci anni nel settore automotive, in Italia e all'estero, dove è venuto a contatto con le pratiche Agile.

         Da tre anni lavora in Argotec come Softwares Engineer e, appassionato dei principi e delle pratiche Agile, insieme ai suoi colleghi sta cercando la giusta strada per applicare tali concetti e metodologie per migliorare i processi di sviluppo di prodotti spaziali, non solo per quanto riguarda il software, ma i sistemi nella loro interezza.`,
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/niccol%C3%B2-battezzati-phd-6915543b/',
        }
      ],
    },
    feliceDeRobertis: {
      img: 'assets/img/speakers/low-res/felice-de-robertis.jpg',
      imgAlt: 'Speaker Felice de Robertis',
      name: 'Felice de Robertis',
      summary: 'inspearit - Agile@Scale Transformation Coach - SAFe SPC',
      bio: [
        `Felice ha 25 anni di esperienza nello sviluppo di sistemi IT in ambito Telco, Automotive, Travel&Leisure, Services e Banking.
        
        La sua esperienza anche internazionale nell'adozione di metodologie Agili è decennale e, come SAFe Practice Consultant, si specializza nella trasformazione di grosse organizzazioni e nello scaling Agile.
        
        In inspearit da oltre sei anni, aiuta quotidianamente le aziende ad ottenere il successo attraverso l'adozione delle metodologie Agili.
        
        Genovese doc, Felice divide la sua vita con la sua famiglia tra Riviera Ligure e Lago di Como, ed oltre ad essere presidente di una ASD calcistica, si diletta nella costruzione di canoe.`,
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/felice-de-robertis-1b76aa17/',
        }
      ],
    },
    nicolaLobusto: {
      img: 'assets/img/speakers/low-res/nicola-lobusto.jpg',
      imgAlt: 'Speaker Nicola Lobusto',
      name: 'Nicola Lobusto',
      summary: 'Agile Coach - SAFe SPC',
      bio: [`
      Nicola è uno Scrum Master e Agile Coach con diversi anni di esperienza nel mondo dell'Agile.

      Laureato in Ingegneria Elettronica al Politecnico di Bari inizia la sua carriera lavorativa più di 20 anni fa come sviluppatore software per la gestione di progetti in regime ISO 9000, occupandosi in seguito di sviluppo Web e di applicazioni mobile. 
      
      Nel 2016 segue un corso Scrum e si innamora della filosofia Agile e Lean Thinking. Questa passione lo spinge ad intraprendere il ruolo di Scrum Master prima e Agile Coach dopo. Nicola ha facilitato diversi team impegnati nello sviluppo software di progetti complessi e tenuto talk e workshop.
      
      Dal 2017 è membro dell'Agile Community di Torino con la quale organizza Meetup ed eventi con l'intento di diffondere l'Agile mindset e di promuovere lo scambio di idee ed esperienze. Nel 2018 diventa socio dell'Italian Agile Movement.

      Miglioramento continuo, Agile e il pensiero snello sono diventati il focus.
      `],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/nicola-lobusto-6ab4a333/',
        },
      ],
    },
    valeriaBurdi: {
      img: 'assets/img/speakers/low-res/valeria-burdi.jpeg',
      imgAlt: 'Speaker Valeria Burdi',
      name: 'Valeria Burdi',
      summary: '🏴‍☠️ Pirate 🤸‍♀️ Rebel 🌪 Trouble Maker SCRUM MASTER & P.O. Junior Digital Product Manager',
      bio: [
        `Docente abilitata di Lingua Inglese per la Scuola Secondaria di I e II Grado. Rimodulando il proprio percorso lavorativo ha intrapreso la strada della consulenza e della formazione aziendale, abbracciando il mindset Agile, trasferendo il bagaglio di competenze in questo settore.

        Scrumaholic and OKR enthusiast.

        Autrice dell'articolo 'Ready-Steady-Sprint: Quando Scrum incontra la didattica' e di 'Una Retrospettiva fra i banchi di scuola' su Agile Italia Magazine.`
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'http://www.linkedin.com/in/valeriaburdi',
        },
      ],
    },
    carmelaFlaminio: {
      img: 'assets/img/speakers/low-res/carmela-flaminio.jpg',
      imgAlt: 'Speaker Carmela Flaminio',
      name: 'Carmela Flaminio',
      summary: 'Studio Avv. Carmela Flaminio',
      bio: [
        `Carmela Flaminio, ha conseguito la laurea in Giurisprudenza nel 2001 presso l'Università degli Studi di Bari, dal 2005 è avvocato e consulente aziendale.
        
        Attenta ai temi della salute e del benessere sociale oltre che della famiglia e dell'individuo, nel 2008 ha partecipato ad un progetto di ricerca sulla Corporate Social Responsability e sulla “sostenibilità” mettendo in risalto le capacità di un'organizzazione di continuare in maniera duratura nel tempo le proprie attività tenendo in debita considerazione l'impatto che queste ultime hanno sul capitale umano.
        
        Di recente ha approfondito gli studi in Economia Aziendale e apprezzato nel corso delle ricerche sul tema del lavoro “agile” gli effetti benefici di una dimensione edonica dell'azienda perché “dipendenti felici sono più produttivi”.`
      ], 
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/carmela-flaminio-5a2361115/',
        },
      ],
    },
    paoloPustorino: {
      img: 'assets/img/speakers/low-res/paolo-pustorino.png',
      imgAlt: 'Speaker Paolo Pustorino',
      name: 'Paolo Pustorino',
      summary: 'Founder & Head of HR @ SparkFabrik',
      bio: [
        `Informatico "fin da piccolo", musicista e filosofo della domenica. Mi piace definirmi un artigiano, creare cose e farle funzionare.

        Ho fondato SparkFabrik con persone che condividono i valori di qualità, trasparenza e impegno.
        Quando parlo di agilità mi sento molto Renè Ferretti.
        
        Nel tempo libero suono, riparo cose e sviluppo videogame.`
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/paolo-pustorino-52267742/',
        },
      ],
    },
    matteoTortelli: {
      img: 'assets/img/speakers/low-res/matteo-tortelli.jpg',
      imgAlt: 'Speaker Matteo Tortelli',
      name: 'Matteo Tortelli',
      summary: 'Designer',
      bio: [
        `Mi piace analizzare problemi per trovare soluzioni, dimostro questa mia passione progettando interfacce grafiche e scrivendo righe di codice.

        Sono curioso e metodico, qualità che mi incitano alla formazione continua leggendo libri o blog e sperimentando nuove tecnologie.`
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/matteonaif/',
        },
      ],
    },
    valerioAcerbis: {
      img: 'assets/img/speakers/low-res/valerio-acerbis.jpeg',
      imgAlt: 'Speaker Valerio Acerbis',
      name: 'Valerio Acerbis',
      summary: 'Frontend Developer',
      bio: [
        `Mi chiamo Valerio, amo tutto ciò che è tecnologico e innovativo ma sono irrimediabilmente attratto da oggetti vintage, dalla loro meccanica e dalla storia che hanno da raccontare.
        
        Suono il pianoforte da quando sono bambino, pratico Snowboard e Kite Surf sul lago di Garda dove vivo.`
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/valerio-acerbis/',
        },
      ],
    },
    carloDiDomenico: {
      img: 'assets/img/speakers/low-res/carlo-di-domenico.jpg',
      imgAlt: 'Speaker Carlo Di Domenico',
      name: 'Carlo Di Domenico',
      summary: 'Reale Mutua - Scrum Master',
      bio: [
        `Nato a Benevento nel 1980, studia Ingegneria dell'Automazione e si trasferisce a Torino nel 2007.
        
        Dopo una lunga esperienza di project management, si dedica alle tematiche Agile concentrando gli sforzi per intraprendere la carriera di Scrum Master e Agile Coach.

        Da sempre appassionato di giochi da tavolo, cofondatore di un'associazione ludica, crede che i Serious Game siano un ottimo strumento per sperimentare e diffondere i principi alla base dell'Agile, come la facilitazione e la buona comunicazione.`
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/carlo-di-domenico/',
        }
      ],
    },
    deborahGhisolfi: {
      img: 'assets/img/speakers/low-res/deborah-ghisolfi.png',
      imgAlt: 'Speaker Deborah Ghisolfi',
      name: 'Deborah Ghisolfi',
      summary: 'Agile Marketing Consultant',
      bio: [
      `Agile Marketing Trainer and Consultant| ICF Executive Coach | Book Author Agility in Marketing | Mentor @UniCatt

      Owner and Founder of Agile Marketing Italia the only Italian community of practices focused on Agility in marketing.
      
      Do you want to Transform you Marketing Organization to do the Right Work at the Right Time for the Right business Goal, at Scale?Agile ways of working is the answer - if tailored to marketing - create shared purpose, aligned goals, and efficient processes.
      
      As ICF Members I deliver Executive Coaching sessions; crucial for managers who want to increase performance and overcome the challenges of managing time, relationships and business objectives.`
      ],
      social: [
        {
          icon: 'bi bi-twitter',
          url: 'https://twitter.com/DeborahGhisolfi',
        },
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/deborahghisolfi/',
        },
        {
          icon: 'bi bi-globe-americas',
          url: 'https://www.agilemarketingitalia.com/',
        }
      ],
    },
    cassandraCavalleri: {
      img: 'assets/img/speakers/low-res/cassandra-cavalleri.jpeg',
      imgAlt: 'Speaker Cassandra Cavalleri',
      name: 'Cassandra Cavalleri',
      summary: 'ICP-MKG Certified Agile Marketer | Scrum Master| Communication e Social Strategist',
      bio: [
        `CIAO, SONO CASSANDRA: Ti affianco come Agile Marketer e Communication Strategist nella creazione di strategie di comunicazione sui social e sul web, nella gestione di pagine e nella creazione di contenuti efficaci. Come? Con un Mindset Agile, Umano ed Empatico
        LE MIE PAROLE DEL CUORE:

        ✔︎ COMUNICAZIONE EMPATICA
        ✔︎ MARKETING HUMAN TO HUMAN
        “In qualsiasi ambito ci troviamo, ricordiamoci che comunichiamo A e CON persone”. Questo mantra mi guida e mi accompagna in tutti i progetti. Affronto ogni sfida con il sorriso e un approccio empatico costruito grazie alla mia formazione umanistica, affiancata a quella più tecnica. Amo gli algoritmi ma preferisco le persone.

        ✔︎ APPROCCIO STRATEGICO
        Affronto ogni progetto di comunicazione e formazione, con un approccio strategico guidato da obiettivi. Collaboro con team di lavoro che utilizzano e diffondono la Metodologia Agile nel business e questo sta forgiando il mio Mindset. (Scopri di più nell'area delle mie esperienze lavorative)`
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/cassandra-cavalleri/',
        },
        {
          icon: 'bi bi-globe-americas',
          url: 'https://www.agilemarketingitalia.com/',
        }
      ],
    },
    andreaGelli: {
      img: 'assets/img/speakers/low-res/andrea-gelli.jpg',
      imgAlt: 'Speaker Andrea Gelli',
      name: 'Andrea Gelli',
      summary: 'Agile-Lean enthusiast',
      bio: [
        `Sono un DJ producer, appassionato di cinema, grande fan delle metodologie agili e della filosofia Lean.
        
        Al momento ricopro il ruolo di Improvement Product Evangelist (non me ne voglia Guy Kawasaki) ed ogni giorno mi interrogo su come coniugare prospettive non convenzionali con le esigenze day-by-day del business.
        
        Mi sento in uno stato di apprendimento permanente e credo che esistano sempre modi migliori per lavorare con le persone, gestire prodotti e generare valore.`
        ],
      social: [
        {
          icon: 'bi bi-twitter',
          url: 'https://www.twitter.com/',
        },
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/company/90877249',
        },
      ],
    },
    claudioSaurin: {
      img: 'assets/img/speakers/low-res/claudio-saurin.jpg',
      imgAlt: 'Speaker Claudio Saurin',
      name: 'Claudio Saurin',
      summary: 'Saber di Caludio Saurin & C - Digital Transformation Consultant & Agile Hardware Evangelist',
      bio: [
        `Per più di 20 anni ho rivestito il ruolo di direttore tecnico in diverse aziende manifatturiere
        Con i miei team e, abbiamo sviluppato modalità agile più di 50 prodotti industriali dotati di software di automazione.


        Ho costituito una “Mini-Fabbrica Agile” con oltre 190 persone.       
        Oggi affianco le aziende nello sviluppo agile dei nuovi prodotti e nella necessaria trasformazione organizzativa

        Su di me…
        - Ogni mattina pratico mindfulness.
        - Amo camminare nella natura e intraprendere viaggi itineranti senza un percorso definito.
        - Sono un appassionato di cinema e un vorace lettore di saggi.`
      ],
      social: [
        {
          icon: 'bi bi-twitter',
          url: 'https://twitter.com/claudesauro',
        },
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/claudio-saurin/',
        },
        {
          icon: 'bi bi-instagram',
          url: 'https://www.instagram.com/claudio_saurin/',
        },
        {
          icon: 'bi bi-globe-americas',
          url: 'https://www.fabbricaagile.it/',
        },
      ],
    },
    francescoRacanati: {
      img: 'assets/img/speakers/low-res/francesco-racanati.jpg',
      imgAlt: 'Speaker Francesco Racanati',
      name: 'Francesco Racanati',
      summary: 'Agile Coach & Trainer',
      bio: [
        `Sono Kanban Trainer Accreditato Kanban University. 
        
        Dal 2013, durante le mie esperienze in Italia e Brasile, ho avuto modo di lavorare nel controllo di gestione, son passato al business operation e poi ho scoperto il mondo del product management.
        
        Nel 2018 ho iniziato il mio percorso come Scrum Master per poi approdare in Inspearit in qualità di Evolutionary Change Coach e non riesco proprio a smettere!
        Piccolo curiosità su di me: sono un "serious game addicted"!`
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/francescoracanati/',
        },
      ],
    },
    annaDeambrosis: {
      img: 'assets/img/speakers/low-res/anna-deambrosis.jpg',
      imgAlt: 'Speaker Anna Deambrosis',
      name: 'Anna Deambrosis',
      summary: 'Reale Mutua - Head of change Management of Reale Group',
      bio: [
        `Ho lavorato per circa 1/4 di secolo nel business assicurativo Vita e Salute occupandomi di prodotti.
        
        Nell'ultima parte di questi 25 anni ho anche ricoperto la carica di amministratore delegato di Blue Assistance, società di servizi del Gruppo.
        
        Dal 2021 sono Head of Change Management di Reale Group. Conosco quindi bene ciò che mi è chiesto di cambiare!
        
        Intuire, immaginare i cambiamenti e come fare diversamente le cose è la curiosità che mi nutre da sempre.
        Cercare o dare un senso, sempre positivo, ai cambiamenti è la mia attività di tutti i giorni.`
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/anna-deambrosis-a8bb5424',
        },
      ],
    },
    federicaDante: {
      img: 'assets/img/speakers/low-res/federica-dante.png',
      imgAlt: 'Speaker Federica Dante',
      name: 'Federica Dante',
      summary: 'Scrum master',
      bio: [
        `Sono una scrum master con il pallino della cultura classica, che amo rivedere nelle pratiche agili.
        Ricopro il ruolo da quasi 5 anni.
        Mi diverto moltissimo nel mio ruolo.

        Sono interessata alla parte giocosa, umana e del learning by doing.
        Mi piace bere e mangiare perchè sono veneta DOC.`
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/federica-dante-b4912a105/',
        },
      ],
    },
    cosimoPaletta: {
      img: 'assets/img/speakers/low-res/cosimo-paletta.png',
      imgAlt: 'Speaker Cosimo Paletta',
      name: 'Cosimo Paletta',
      summary: 'RTE, Wolters Kluwer',
      bio: [
        `Started my career as Software Engineer and proceeding then as Tech Project Manager and Scrum Master.
        
        Passion about lean ethodologies led me to continue on this career path.
        
        I Currently work as RTE and People Manager at Wolters Kluwer, Tax Division, with the aim to support organization made by 100+ people working on new Cloud solution.`
      ],
    },
    emilianoSoldi: {
      img: 'assets/img/speakers/low-res/emiliano-soldi.jpg',
      imgAlt: 'Speaker Emiliano Soldi',
      name: 'Emiliano Soldi',
      summary: 'Agile Enterprise Transformation Coach',
      bio: [
        `Emiliano è un professionista appassionato, che lavora in ambito IT e consulenza, da oltre venti anni.

        Nasce come sviluppatore nei primi anni novanta. Ha poi guidato per quindici anni una società di consulenza e sviluppo software; periodo in cui è entrato in contatto diretto con approcci metodologici come Lean, Agile e di Change Management.
        Competenze che ha consolidato nel tempo come Agile Enterprise Coach, facilitando diverse trasformazioni agili in ambiti Banking, Energy, Telco, Pharma, Automotive.
        Oggi ricopre il ruolo di Business Agility Practice Lead in una primaria società di consulenza.
        
        Ha partecipato come speaker a conferenze in italia e all'estero su temi inerenti Agile e Business Agility, Leadership e Project Management.`
      ],
      social: [
        {
          icon: 'bi bi-twitter',
          url: 'https://twitter.com/AgileTriathlete',
        },
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/emilianosoldi/',
        },
        {
          icon: 'bi bi-globe-americas',
          url: 'https://www.emilianosoldi.it/',
        },
      ],
    },
    nicolaMoretto: {
      img: 'assets/img/speakers/low-res/nicola-moretto.jpeg',
      imgAlt: 'Speaker Nicola Moretto',
      name: 'Nicola Moretto',
      summary: 'QMates, CEO & Co-Founder',
      bio: [
        `Nicola Moretto, CEO e Co-founder di QMates, azienda neo nata che ha come missione il mettere a disposizione delle startup le pratiche tecniche dell'agilità.
        
        Nicola ha un passato da sviluppatore software con pratiche XP e da agile coach esperto di scaling organisation in enterprise e startup.`
      ],
      social: [
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/in/nicola-moretto-ba197040/',
        },
      ],
    },
    emptyTemplate: {
      img: 'assets/img/speakers/low-res/.jpg',
      imgAlt: 'Speaker ',
      name: '',
      summary: 'world',
      bio: ['', '', ''],
      social: [
        {
          icon: 'bi bi-twitter',
          url: 'https://www.twitter.com/',
        },
        {
          icon: 'bi bi-linkedin',
          url: 'https://www.linkedin.com/company/90877249',
        },
      ],
    },
  };
})();
