(function () {
  'use strict';

  window.TAC_ORGANIZERS = {
    danielPalmisano: {
      img: 'assets/img/organizers/low-res/daniel_palmisano.jpg',
      imgAlt: 'Organizzatore Daniel Palmisano',
      name: 'Daniel Palmisano',
      link: 'https://www.linkedin.com/in/pleinad'
    },
    monicaCarpaneseBeggiato: {
      img: 'assets/img/organizers/low-res/monica_carpanese_beggiato.jpg',
      imgAlt: 'Organizzatore Monica Carpanese Beggiato',
      name: 'Monica Carpanese Beggiato',
      link: 'https://www.linkedin.com/in/monica-carpanese-beggiato-875aab20'
    },
    giovanniMessina: {
      img: 'assets/img/organizers/low-res/giovanni_messina.jpg',
      imgAlt: 'Organizzatore Giovanni Messin',
      name: 'Giovanni Messina',
      link: 'https://www.linkedin.com/in/giovanni-messina-0935b872'
    },
    almaDelCampo: {
      img: 'assets/img/organizers/low-res/alma_del_campo.jpg',
      imgAlt: 'Organizzatore Alma Maria Del Campo Vara',
      name: 'Alma Maria Del Campo Vara',
      link: 'https://www.linkedin.com/in/alma-maria-del-campo-vara-1497781a'
    },
    cinziaMarando: {
      img: 'assets/img/organizers/low-res/cinzia_marando.jpg',
      imgAlt: 'Organizzatore Cinzia Marando',
      name: 'Cinzia Marando',
      link: 'https://www.linkedin.com/in/cinzia-marando-consulente-marketing-comunicazione-formazione'
    },
    giadaPallavicini: {
      img: 'assets/img/organizers/low-res/giada_pallavicini.jpg',
      imgAlt: 'Organizzatore Giada Pallavicini',
      name: 'Giada Pallavicini',
      link: 'https://www.linkedin.com/in/giada-pallavicini-13659861'
    },
    nicolaLobusto: {
      img: 'assets/img/organizers/low-res/nicola_lobusto.jpg',
      imgAlt: 'Organizzatore Nicola Lobusto',
      name: 'Nicola Lobusto',
      link: 'https://www.linkedin.com/in/nicola-lobusto-6ab4a333'
    },
    stefanoMarello: {
      img: 'assets/img/organizers/low-res/stefano_marello.jpg',
      imgAlt: 'Organizzatore Stefano Marello',
      name: 'Stefano Marello',
      link: 'https://www.linkedin.com/in/stefano-marello-53158452/'
    },
    antonioAbbasciano: {
      img: 'assets/img/organizers/low-res/antonio_abbasciano.jpg',
      imgAlt: 'Organizzatore Antonio Abbasciano',
      name: 'Antonio Abbasciano',
      link: 'https://www.linkedin.com/in/antonio-abbasciano-17b348131/'
    },
    emptyTemplate: {
      img: 'assets/img/organizers/low-res/.jpg',
      imgAlt: 'Organizzatore  ',
      name: '',
      link: ''
    }
  };
})();
